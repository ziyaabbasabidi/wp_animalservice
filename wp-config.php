<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'animalservice');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-{dxiXPk(y=9yv{F|0}-Jz#/?3`AS$h}jgjlHk)sq);ZR43(^&?T/i$lA@Yg>a(S');
define('SECURE_AUTH_KEY',  'Fuy:R1yT{5.&k?T:]r2Ar%uxK4@$e;Mo*=:nOZ,q(%lc1L/;w;p>qNP,Erf4}p9|');
define('LOGGED_IN_KEY',    'bl9(6L{XYK^%N&5{N!@Rt#TFHmQI&I-Ih^G#$9`O;CxhX[zt@dXnuXr{aNgZ{iG&');
define('NONCE_KEY',        'uvY. /,u}}]PY%KOJcM16{k9+`7~XGOPYz()@?h2K lI6jlD7wH*3~>IzC*N:$jR');
define('AUTH_SALT',        '0DA@$}_.qyseSy<~M66xvG5|h#}50h76<K7!7UrH9=m[DC!%x8BJPuAoSP3K>1)v');
define('SECURE_AUTH_SALT', '8?`01$iH55;wl|V?`H%F>E8mdt[dS.eh]r=_8,Rn}OM=u,.pvv@VCPlC5wIK$i?c');
define('LOGGED_IN_SALT',   'O3j|EON|)ch-*_8_zo+>D ifrO!9et|l,ip,+oB:|Pk|?I,b;6Dq1noN*ANT^H={');
define('NONCE_SALT',       'a^$aa-hwpMw/jsim6b6EH>SeyXDedu3R2qaw@lq!b5ZY>5He1RU]vLx2QV]%,k?p');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
